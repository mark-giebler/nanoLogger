/**
 ******************************************************************************
 * @file           log.c
 * @brief          NanoLogger - a compact embedded logging system.
 ******************************************************************************
 *
 *  Created on: Mar-02-2013
 *      @author Mark Giebler
 *
 * https://gitlab.com/mark-giebler/nanoLogger
 *
 * NanoLogger a compact embedded logging system.
 * A RAM memory frugal event and data logging system for embedded systems.
 * This logger was created for a home project:  Mark's 20,000KM Garage Door Controller project.
 *
 * By Mark Giebler mark.giebler-VIA-gmail---com
 *
 * 2013-03-02
 *
 * This log file code is open source, Apache 2.0 license.
 */
#include "log.h"

//#include <cmsis_gcc.h>	// my modified version with CRITICAL_REGION( CRITICAL_RESTORESTATE )

/* ------------------------------------------------------------------------- */
/* -------------------------- Log File --BEGIN------------------------------ */
/* ------------------------------------------------------------------------- */
#define LogStrG(aTxt) #aTxt	/* SPAM reduction obfuscation */
#define __YO__ LogStrG(@)
#define __ZO__ LogStrG(.)
__attribute__ ((used)) const char NanoLoggerCopyright[] = "NanoLogger by mark" __ZO__ "giebler" __YO__ LogStrG(gmail) __ZO__ "com Apache 2.0 License";


// Define times for the clock used for logging
#define LOG_SECONDS	(1000ul)	// number of system ticks per second.
#define LOG_MINUTES (60ul * LOG_SECONDS)
#define LOG_HOURS	(60ul * LOG_MINUTES)

#define LOG_HEARTBEAT_TIME	(12ul*LOG_HOURS)	// milliseconds per heartbeat. currently 12 hours.

// define these next functions based on platform, either here or elsewhere
char * strcpystr(char* dest, char* input, uint16_t* len);
char * IntToAsciiHex(char * dest_string,int min_digits, uint32_t value);
char * IntToAsciiDec(char * dest_string,int min_digits, uint32_t value);

#ifdef LOG_COMPACT
/* number of log entries:. 168 will fit in 2 eeprom segments (256 byte), with 8 bytes left for sig, head and tail pointers. */
#define LOG_FILE_SIZE	168

logEntry_t LogFile[LOG_FILE_SIZE];	// logging data array.
int16_t logHead=0;
int16_t logTail=0;
uint16_t logTicks100ms=0;		// 100mSec tick tracking to detect one second intervals
uint16_t logTime=0;				// one second time clock.
extern uint16_t ticks_100ms;	// in main.c

#endif	/*	#ifdef LOG_COMPACT	*/

#ifndef LOG_COMPACT
/* number of log entries:. */
#define LOG_FILE_SIZE	(400)
#define LOG_TRIM_BACK	(LOG_FILE_SIZE/10)	// trim log back by 10% when it is full. Set to 0 to disable feature.

#ifdef LOG_RESET_PERSISTENT
// log file parameters are placed in non-initialized (.noinit) section of RAM.
// This allows the data to persist across resets. (But not across power cycles POR unless RAM is non-volatile)
// The linker script file is used to create the .nanoLoggerData alignment attribute in the .noinit section
// such that the address of this section can remain constant across firmware versions.

logFile_t logFile __attribute__ ((section (".noinit.nanoLoggerData")));
static logEntry_t logEntry[LOG_FILE_SIZE] __attribute__ ((section (".noinit.nanoLoggerData")));		// logging data array.
#else
// log file is zeroed at reset
logFile_t logFile;
static logEntry_t logEntry[LOG_FILE_SIZE];		// logging data array.

#endif	/*  #ifdef LOG_RESET_PERSISTENT */
#endif	/*	#ifndef LOG_COMPACT	*/

#if( LOG_ENABLE_UPTIME == 1)
int16_t  upTimeHours;	// total up time in hours
int16_t  upTimeDays;	// total up time days
#endif

#ifdef LOG_DESCRIPTIONS
/*!
 * \brief The elements of this log description structure are application specific.
 * Modified enums and description text as needed for application.
 *
 * maps log event number to a descriptive text string.
 * Used for dumping the log for human consumption.
 * The order of elements in this log description structure
 * must be manually coordinated with the logType enum
 *
 * For LOG_COMPACT build:
 * Descriptions must be exactly 20 characters. Pad with space characters ' ' if needed.
 *
 */
struct logdes{
	const uint8_t	event;				//!< logType enum, force to be 8 bits.
	const char*  descStr;				//!< 20 character string.
};

const struct logdes logDescriptionList[]={
	/*					"12345678901234567890"	*/
	{L_NO_OP,			"?                 .."},	//!< Place holder to keep index matching the enum value.
	{L_RESET_POR,		"Power On Reset      "},	//!< Power On Reset
	{L_RESET_PIN,		"Pin Reset     Count:"},	//!< MCU Reset pin initiated reset.
	{L_RESET_SW,		"Software Reset      "},	//!< Software initiated reset.
	{L_IWDG_RESET,		" IWDG: Reset event  "},	//!< IWDG initiated reset.
	{L_SHELL_RESET,		" Shell UI Reset     "},	//!< Reset initiated via Shell UI
	{L_VERSION,			"   Firmware Version:"},	//!< Log firmware version. Example how to log: `logDataRaw(L_VERSION, get_sw_version_int());`
	{L_VERSION_HW,		"  Hardware Revision:"},
	{L_FLASH_OK,		"   Flash OK         "},
	{L_FLASH_INIT,		"   Flash Defaults!  "},
	{L_MAIN_LP_START,	"   State Mch Start  "},
	{L_PPP_UP,			"   Network UP       "},
	{L_PPP_DOWN,		"   Network Down     "},

	{L_ASSERT_FAIL,		"  Assert Fail: Line:"},	//!< Assertion failed.

	{L_ERROR_HANDLER,	"    Error_Handler:  "},	//!< Error_Handler was called.

	{L_LOG_INIT_OK,		"** nanoLOG: Init OK:"},	//!< =0xF0 Logging system init OK
	{L_LOG_CLEARED,		"** Log File Cleared*"},
	{L_LOG_END,			" --- End Of Log --- "},	//!< Marks the end of log.
	{L_TEST_LOG,		".. Logging Test .. ."},	//!< For log testing
	{L_LOG_PAUSED,		"** Logging Paused **"},
	{L_LOG_RESUMED,		"** Logging Resumed *"},
	{L_LOG_REPEATED,	"** Log Event Repeat*"},	//!< Previous log event repeats one or more times.
	{L_LOG_RESERVED,	"nanoLOG: rsrvd      "},
	{L_EVENT_BITS,		"       Event Flags: "},	//!< One or more event flags changed state.
	{L_UNHANDLED,		"No Handler for Event"},	//!< The logging model does not implement the type of function called.

#if (LOG_ENABLE_TYPE_INFO == 1)
	{L_DATABYTES_CSTR,	" [Data] (Const Str) "},	 //!< Data in timestamp word pointer to const char null terminated string.
	{L_DATABYTES_STR,	"   [Data] (Str)     "},
	{L_DATABYTES_HEX,	"   [Data] (HEX)     "},
	{L_DATABYTES,		"   [Data]           "},
#else
	{L_DATABYTES_CSTR,	" "},	// L_DATABYTES_CSTR	=0xFA,
	{L_EVENTCODE_HEX,	" "},	// L_EVENTCODE_HEX	=0xFB,
	{L_DATABYTES_STR,	" "},	// L_DATABYTES_STR	=0xFC,
	{L_DATABYTES_HEX,	" "},	// L_DATABYTES_HEX	=0xFD,
	{L_DATABYTES,		" "},	// L_DATABYTES		=0xFE,	// Decimal format number
#endif
	// --------------  must be last 2  --------------
	{L_HEARTBEAT,		"+++  HeartBeat   +++"},	// L_HEARTBEAT		=0xFF
	{L_NO_OP,			"?                 .."}		// the end ***************
};
#endif	/*	#ifdef LOG_DESCRIPTIONS	*/

#ifndef LOG_COMPACT
/*!
 * gen_logCheckValid() Check if passed in theLog structure is valid.
 *
 * Checks:
 * 	- logSignature
 * 	- logEntry pointer is correct.
 * 	- logSize,
 * 	- logTrimBack is greater than 0 and less than logSize/2
 * 	- logHead and logTail are in range of logSize
 * 	- the tail entry in the log is L_END (Build optiona)
 *
 * Note that if logSignature is equal to ~sig, the log file it temporarily disabled, but still valid.
 *
 * @returns 1 if valid, 0 if invalid
 */
static uint8_t gen_logCheckValid( logFile_t * __restrict__ theLog, uint32_t sig, uint16_t size, logEntry_t logEntry[])
{
#ifdef LOG_RESET_PERSISTENT
	if(theLog == NULL
		|| (theLog->logSignature != sig && theLog->logSignature != ~sig)
		|| (theLog->logEntry != &logEntry[0])
		|| theLog->logSize != size
#if (LOG_TRIM_BACK > 0)
		|| (theLog->logTrimBack <=0 || theLog->logTrimBack >= size/2 )
#endif
		|| theLog->logHead < 0
		|| theLog->logTail < 0
#if (LOG_ENABLE_CHECKCODE)
		|| theLog->checkCode != (((theLog->logHead << 8) ^ theLog->logTail) ^ (uint16_t)(sig))
#endif
#if (LOG_END_EVENT)
		|| theLog->logEntry[theLog->logTail].logEvent != L_LOG_END
#endif
		)
	{
		return 0;	// something is invalid
	}
	// check if pointers just need to be wrapped around.
	if( theLog->logHead >= size )
		theLog->logHead =0;
	if( theLog->logTail >= size )
		theLog->logTail =0;

	return 1;	// valid
#else
	(void) theLog;
	(void) sig;
	(void) size;
	(void) logEntry;
	return 0;	// non-persistent RAM data, so indicate not valid
#endif		// #ifdef LOG_RESET_PERSISTENT
}
/*!
 * Check if logFile structure is valid.
 * @returns 1 if valid, 0 if invalid
 */
static uint8_t logCheckValid(void)
{
	return gen_logCheckValid(&logFile, LOG_FILE_SIG, LOG_FILE_SIZE, &logEntry[0]);
}
/*!
 * Reset logFile structure to valid configuration.
 * For init() and to clear the log file.
 */
void logResetFile(void)
{
	logFile.logSignature = 0;	// mark as invalid
	logFile.logHead=0;
	logFile.logTail=0;
	logFile.logEntry = &logEntry[0];
#if (LOG_ENABLE_CHECKCODE)
	logFile.checkCode = (((logFile.logHead << 8) ^ logFile.logTail) ^ (uint16_t)(LOG_FILE_SIG));
#endif
	logFile.logSize = LOG_FILE_SIZE;
	logFile.logTrimBack = LOG_TRIM_BACK;
	logFile.logSignature = LOG_FILE_SIG;
	// log clear event, will update checkCode.
	logEvent(L_LOG_CLEARED);
}
/*!
 * Initialize the logging system.
 * Note: this is safe to call before RTOS is running.
 *
 * If this is called after a power on event, then the logging RAM structure will be initialized.
 * If after a reset and LOG_RESET_PERSISTENT is defined, the log is kept as-is if it is still in a valid state.
 */
void log_Init(void)
{
	// validate the RAM area.
	if( !logCheckValid() )
	{
		// RAM area needs to be initialized.
		logResetFile();
	}
}


#ifndef LOG_FIXED_STRING_LENGTH
/*!
	* Log an event type and const char string.
	* Uses two log entries.
	*/
void logEventDataConstStr(logType Levent, const char* Ldata)
{
//	CRITICAL_REGION( CRITICAL_RESTORESTATE )	// ISR safe disable interrupts.
	{
	/* --- BEGIN critical region --- */
		logEvent( Levent );
		logDataConstString( Ldata );
	/* --- END critical region --- */
	}
}

/*!
 * Log an event type with number a const string.
 * For logging line number/filename type events.
 * Uses two log entries.
 */
void logEventData16buDecConstString(logType Levent,uint16_t numb, const char * Ldata)
{
//	CRITICAL_REGION( CRITICAL_RESTORESTATE )	// ISR safe disable interrupts. Req's mod'ed: #include <cmsis_gcc.h>
	{
	/* --- BEGIN critical region --- */
		logEventData16buDec( Levent, numb );
		logDataConstString( Ldata );
	/* --- END critical region --- */
	}
}
#endif

#if (LOG_ENABLE_CALLBACKS == 1)

static void (*logCallbacks[4])(uint32_t) = {0,0,0,0} ;
static uint8_t logCbCount=0;
/*!
 * @brief  Register a callback to perform periodic log maintenance for
 * specific log conditions.
 *
 * @param callback pointer to callback function
 * The callback's argument is an uint32_t with the current system tick time.
 *
 * See comment block below for an example callback.
 *
 *
~~~
void example_callback(uint32_t tick )
{
	static uint32_t expireTime=6000;	// 60 second expire time for running logging op or other op.
	if( checkExpired(tick, expireTime) )
	{
		// set new expire time to update in 60 seconds.
		expireTime = tick + (60ul*1000ul);

		// do periodic task here
	}
}

~~~
 *
 * @sa logTimeDo()
 */
void logRegisterCallback(void (*callback)(uint32_t) )
{
	if( logCbCount < sizeof(logCallbacks) / sizeof(*logCallbacks))
	{
		// there is still room for adding a call back
		// check if callback already registered.
		for(uint8_t i = 0; i <  sizeof(logCallbacks) / sizeof(*logCallbacks); i++)
		{
			if(logCallbacks[i] == callback)
				return;	// already registered.
		}
		// not found, thus register it.
		logCallbacks[logCbCount++] = callback;
	}
}


/*!
 * Run all the registered callbacks.
 * Private function called by logTimeDo().
 *
 * @param tickNow current system time tick value.
 */
static void logRunCallbacks(uint32_t tickNow )
{
	for(uint8_t i = 0; i <  sizeof(logCallbacks) / sizeof(*logCallbacks); i++)
	{
		if(logCallbacks[i] == NULL)
			return;
		logCallbacks[i](tickNow);
	}
}
#endif	/* (LOG_ENABLE_CALLBACKS == 1) */

/*!
 * @brief Log file periodic maintenance.
 *
 * Must be Called periodically to manage putting heartbeat messages into the log file.
 * As well as managing any other project specific periodic log maintenance via registered
 * callbacks.
 * The recommended call rate can be on the order of 0.5 to 1 second.  Can be shorter.
 *
 * @param timeNow current system time tick value.
 *
 * @sa logRegisterCallback()
 */
void logTimeDo( uint32_t timeNow )
{
#ifdef LOG_HEARTBEAT
	static uint32_t lastHeartbeat;
	if(timeNow  >= (lastHeartbeat + LOG_HEARTBEAT_TIME))
	{
		logEvent(L_HEARTBEAT);
		lastHeartbeat = timeNow;
	}
#endif
#if (LOG_ENABLE_CALLBACKS == 1)
	logRunCallbacks(timeNow);
#endif

	(void) timeNow;
}

/*!
 * Get maximum number of log entries the log file can hold.
 */
int16_t gen_logGetLogSize(logFile_t * __restrict__ theLog)
{
	return theLog->logSize-1;
}

/*!
 * gen_LogGetEventCount
 *
 * Returns the number of log events currently in theLog.
 */
int16_t gen_logGetEventCount(logFile_t * __restrict__ theLog)
{
	if(theLog == NULL) return 0;
	if( theLog->logTail >= theLog->logHead)
		return theLog->logTail-theLog->logHead;
	// log has wrapped around
	return theLog->logSize + theLog->logTail-theLog->logHead;
}

#endif	/*	#ifndef LOG_COMPACT	*/

#ifdef LOG_COMPACT
/*!
 * Advance log time by one second
 *
 */
void logTimeSync( void)
{
	logTicks100ms = ticks_100ms+10;	// add one second to current systime.
}

/*!
 * log an event with current log time stamp
 */
void logEvent(logType Levent)
{
	logDataRaw( Levent, logTime );
}

/*!
 * Call periodically to update log's elapsed time timer.
 * Assumes system tick time is 100mSec rate.
 * Updates the log system's global logTime (elapsed seconds counter).
 *
 */
void logTimeDo( void)				// update log's one second timer if needed.
{
	if( ticks_100ms == logTicks100ms || ticks_100ms == logTicks100ms+1)
	{
		logTime++;					// another second passed.
		logTicks100ms = ticks_100ms + 10;
		if (logTime == LOG_HEARTBEAT_TIME) 		// 12 hours log a heart beat event.
		{
			logEvent(L_HEARTBEAT);
		}else if( logTime > LOG_HEARTBEAT_TIME)
		{
			logTime = 0;			// wrap around at 12 hours.
			upTimeHours+= 12;		// add 12 hours to up time total.
			if(upTimeHours >= 24)
			{
				upTimeHours=0;
				upTimeDays++;
				logData( upTimeDays );
			}
		}
	}
}

/*!
 * Create an entry in the log structure.
 *
 * Rdata - copied into structure's timestamp
 * Levent - copied into structure's logEvent.
 */
void logDataRaw(logType Levent, uint16_t Rdata)
{
	LogFile[logTail].logEvent = Levent;
	LogFile[logTail++].timeStamp = Rdata;
	if(logTail >= LOG_FILE_SIZE)
		logTail = 0;				// wrap around
	if(logHead == logTail)
	{
		logHead++;					// tail chasing head.
		if(logHead >= LOG_FILE_SIZE)
			logHead = 0;
	}
	// set end flag:
	LogFile[logTail].logEvent = L_NO_OP;
	LogFile[logTail].timeStamp = logTime;
}
/*!
 * log data into the log (uses timestamp for data storage)
 * Data logged such that it will be displayed as a decimal number
 * Normally used following a LogEvent() call to provide data associated with the event.
 */
void logData(uint16_t Ldata)
{
	logDataRaw( L_DATABYTES, Ldata );
}
/*!
 * log data into the log (uses timestamp for data storage)
 * Data logged such that it will be displayed as a hexadecimal number
 * Normally used following a LogEvent() call to provide data associated with the event.
 */
void logDataHex(uint16_t Ldata)
{
	logDataRaw( L_DATABYTES_HEX, Ldata );
}
/*!
 * log an event with pertinent data.
 * Data logged such that it will be displayed as a decimal number
 */
void logEventData(logType Levent, uint16_t Ldata)
{
	logEvent( Levent );
	logData( Ldata );
}
/*!
 * log an event with pertinent data.
 * Data logged such that it will be displayed as a hexadecimal number
 */
void logEventDataHex(logType Levent, uint16_t Ldata)
{
	logEvent( Levent );
	logDataHex( Ldata );
}


/*!
 * log an event with a string to the log structure.
 * String is truncated at 10 characters.
 * Each character consumes one log entry.
 */
void logEventDataStr(logType Levent, char* str)
{
	uint16_t* data;
	char i = 0;
//	LogEvent( Levent );
	data = (uint16_t*) str;
	while (i++ < 10)
	{
		logDataRaw( L_DATABYTES_STR, *data++ );
	}
}
/*!
 * logEventCount
 *
 * Returns the number of log events currently in the log.
 */
int16_t logGetEventCount(void)
{
	if( logTail > logHead)
		return logTail-logHead;
	return LOG_FILE_SIZE-1;		// log is full.
}
#endif	/*	#ifdef LOG_COMPACT	*/

/*!
 * logGetDescription
 *
 * returns pointer to null terminated string describing the passed in log event code.
 * Looks up the description in the @ref logDescriptionList[] structure.
 */
const char * logGetDescription(logTypeEvent levent)
{
#ifdef LOG_DESCRIPTIONS
	uint16_t itm=L_NO_OP+1;	// skip the L_NO_OP element.
	// try quick look up.
	// This requires the index of the logDescriptionList to match the logType.
	if(levent < sizeof(logDescriptionList)/sizeof(struct logdes)
			&& logDescriptionList[levent].event == levent)
		return logDescriptionList[levent].descStr;
	// need slower look up.
	while(logDescriptionList[itm].event != L_NO_OP && logDescriptionList[itm].event != levent)
		itm++;
	return logDescriptionList[itm].descStr;
#else
	return 0;
#endif
}

#ifndef LOG_COMPACT
// non-compact log  ***********************************************************

/*!
 * Get a pointer to the data array containing the N-th item in the log,
 * also returns in *p_size the size of the entry in bytes.
 * Used to serialize data for writing to other device,
 * e.g. non-volatile storage, serial port, etc.
 */
uint8_t* logGetEntry(int16_t item, int16_t* p_size)
{
	*p_size = sizeof(logEntry_t);
	// note: wrap around is not handled. To keep code size down.
	//item += logFile.logHead;
	// check if no timestamp and adjust size as needed.
	if( logFile.logEntry[item].logData24b & LOG_NO_TIMESTAMP )
		*p_size -= sizeof(uint32_t);
	return (uint8_t*)&(logFile.logEntry[item]);
}


/*!
 * gen_logDataRaw
 * Generic function for any logFile type log structure.
 * Puts data into one element of the log file structure object.
 *
 * @param Levent - is logType event type
 * @param Rdata - is normally timestamp tick, but can be 32 bit data for data only entries.
 * @param logData24b - 16 bit data plus flags.  set logData24b to 0 if not used.
 * @param theLog - points to the logFile_t structure in RAM
 */
void gen_logDataRaw(logType Levent, uint32_t Rdata, uint32_t data24b, logFile_t * __restrict__ theLog)
{
	// sanity check: make sure logFile is initialized (or logging might be paused if sig == ~LOG_FILE_SIG)
	if( theLog->logSignature != LOG_FILE_SIG)
		return;

	register int16_t tail;			// use registers to maximize speed.
//	CRITICAL_REGION( CRITICAL_RESTORESTATE )	// ISR safe disable interrupts. Req's mod'ed: #include <cmsis_gcc.h>
	{	/* --- BEGIN critical region --- */
		tail = theLog->logTail;
		register logEntry_t* logEntry = theLog->logEntry;
		logEntry[tail].logEvent = Levent;	// new entries at the tail
		logEntry[tail].logData24b=data24b;	// set logData24b to 0 if not used.
		if(!(data24b&LOG_NO_TIMESTAMP))		// timestamp if needed.
			logEntry[tail].timeStamp = Rdata;
		tail++;
		if(tail >= theLog->logSize)
			tail = 0;				// wrap around
		theLog->logTail = tail;

		if(theLog->logHead == tail)
		{
			// log at maximum capacity.
#if (LOG_TRIM_BACK > 0)
			// trim the log back by a percentage of elements.
			// That way when the log is full we only trim back every 20th+ log call.
			theLog->logHead += theLog->logTrimBack;			// trim the log
#else
			// trim one element
			theLog->logHead++;
#endif
			if(theLog->logHead >= theLog->logSize)
				theLog->logHead -= theLog->logSize;			// wrap around
		}
	}	/* --- END critical region --- */
}


#if LOG_HAS_TEST_METHODS
/*!
 * gen_logTestFillEmpty()
 * Fill empty space in the log with test entry with incrementing test number
 *
 */
void gen_logTestFillEmpty(logFile_t * __restrict__ theLog)
{
	int16_t nmb = logGetLogSize() -  logGetEventCount();
	while( nmb > 0 )
	{
		gen_logDataRaw(L_TEST_LOG, logTime, LOG_D16B_USED | (((uint32_t) nmb--)&0x0ffff), &logFile);
	}
}
#endif

#define LOG_LINE_LENGTH 48		//!< Nominal length of a log line. @sa logGetStringLength()

/*!
 * LogGetStringLength
 *
 * returns how long the log string will be for the requested number of log entries + uptime header line.
 * - If nmbr == 0 then just returns the length of one log line
 *
 * - Each log entry is minimum of 40 bytes long: "hhhh:mm:ss.ttt XX .........20.........\n"
 * - 18 for timestamp prefix +20 for message +1 to EOL char
 * - 40 is the length of the first header line (Uptime).
 * - Does Not include NULL termination.
 *
 * @param nmbr - number of log entries to request the length of.
 *
 * @note This is may not be accurate due to ability to log pointer to const char string which may be of any length.
 *
 */
int16_t logGetStringLength(int16_t nmbr)
{
	if(nmbr==0)
		return LOG_LINE_LENGTH;			// don't include header length.
	return (nmbr*LOG_LINE_LENGTH)+2*LOG_LINE_LENGTH;	// includes "uptime" header.
}

/* includes and macros for logGetString()  */
#include <string.h>
#include <stdio.h>
#include <inttypes.h>		/* for printf macros PRIu32, PRIx32, etc */

#ifndef s_EOL
#define s_EOL "\n"
#endif
/*!
 * logTimeToTimeString
 * convert uin32_t tick time to string.
 *
 * get the a system tick time into a formatted string:
 * ~~~
 * <prefix>hhhh:mm:ss.ttt<suffix>
 * where:
 * <prefix> is an option prefix string.
 * hhhh -  hours (1038 max)
 * mm	-  minutes
 * ss	- seconds
 * ttt	- milliseconds
 * <suffix> is an optional suffix string.
 * ~~~
 * @param  tm - tick time in milliseconds
 * @param  prefix - prefix string
 * @param  inStr - buffer to copy the string to
 * @param	strSz - size of the buffer
 * @param  suffix - suffix string
 * @retval number of bytes written to the inStr buffer. Error if < 0  or >= strSz
 */
int logTimeToTimeString(uint32_t tm, char* prefix, char* inStr, int strSz, char* suffix)
{
	uint16_t hh,mm,ss,ttt;	// hh - hours, mm - minutes, ttt - milliseconds
	int ix=0;
	// output "hhhh:mm:ss.ttt"	// 14 characters
	ttt = tm %1000;		// miliseconds part.
	tm = tm/1000;		// timestamp to seconds.
	hh = tm /(60*60);	// hours
	tm -= (hh*3600);	// remove the hours
	mm = tm/60;			// minutes
	ss = tm - (mm*60);	// remaining is seconds.
	ix=snprintf(inStr, strSz, "%s%04" PRId16 ":%02" PRId16 ":%02" PRId16 ".%03" PRId16 "%s",prefix, hh,mm,ss,ttt,suffix);
	return ix;
}

/*!
 * logGetCurrentTimeString
 *
 * get the current system tick time into a formatted string:
 *
 * "\nhhhh:mm:ss.ttt Current System     "
 *
 * 		hhhh - hours (1038 max - 43.25 days max)
 * 		mm	- minutes
 * 		ss	- seconds
 * 		ttt	- milliseconds
 *
 * @param  inStr - buffer to copy the string to
 * @param	strSz - size of the buffer
 * @retval number of bytes written to the inStr buffer. Error if < 0  or >= strSz
 */
int logGetCurrentTimeString(char* inStr, int strSz)
{
	return logTimeToTimeString(logTime, s_EOL ,inStr, strSz," <--Current SysTime  ");
}
/*!
 * gen_logGetString
 *
 * Builds ASCII string from log data placing string in str[].
 *
 * Starting at log entry 'begin' for nmbr entries or until stopn chars copied.
 *
 * 		if -1, start with current timestamp (uptime) header: <TmStamp>"  System Uptime     " "Logs: 000 of 000    ",
 * 		if -2 output a timestamp footer and exit.
 *
 *
 *
 * @param str - buffer to copy log info to.
 * @param stopn has the size of the passed in string buffer str[].
 * @param theLog has the log file object.
 *
 * Each log entry description is a minimum of 40 bytes long: "\nhhhh:mm:ss.ttt XX .........20........."
 *
 *		 " XX" is option - hex for the LegEvent code.
 * 		18 for timestamp prefix + 20 for message +1 to EOL char
 *
 * Returns log entry we stopped at (== nmbr when done.)
 *   subtracts number of bytes copied from *stopn (thus *stopn indicates unused space in buffer).
 *
 * Not thread safe! Should be called within OS critical region protection.
 * Or logging should be paused before calling.
 *
 * @sa logPause()
 * @sa logTimeToTimeString()
 * @sa logGetCurrentTimeString()
 */
int16_t gen_logGetString(int16_t begin, int16_t nmbr, char* str, int16_t * stopn, logFile_t * __restrict__ theLog )
{
	int16_t item;
	uint32_t tm;			// tm - timestamp
	uint16_t hh,mm,ss,ttt;	// hh - hours, mm - minutes, ttt - milliseconds
	char * strptr;
	int ix=0;

	// sanity check
	if(str == NULL || *stopn <= 50)
		return begin;

	// Adjust available buffer size to be one less that actual size to allow for guaranteed NULL termination.
	(*stopn)--;

	strptr=str;

	if( begin == -1 || begin == -2)
	{
#if( LOG_ENABLE_UPTIME == 1)
		if( begin == -1)
		{
			// put in uptime header
			// "\nUptime: Days:0000 Hours:0000         \n"
			ix=snprintf(strptr, (*stopn), s_EOL"Uptime: Days:%04" PRId16 " Hours:%04" PRId16 "         ", upTimeDays,upTimeHours);
			if( ix < 0 || ix >= (*stopn)){
				// problem encountered
				goto commonExit;
			}
			strptr += ix;
			*stopn -= ix;				// subtract off what we copied so far.
		}
#endif
		ix = 0;
		if( begin == -1 )
		{
			// log uptime
			ix = logTimeToTimeString(logTime, s_EOL ,strptr, (*stopn),"  System Uptime.     ");
		}else
		if( begin == -2 )
		{
			// log current tick timestamp
			ix = logGetCurrentTimeString(strptr, (*stopn));
		}
		if( ix < 0 || ix >= (*stopn))
		{
			// buffer problem encountered
			goto commonExit;
		}
		strptr += ix;
		*stopn -= ix;				// subtract off what we copied so far.
		if (begin == -1 )
		{
			//"Logs: 000 of 000    "
			ix=snprintf(strptr, (*stopn), "Logs: %03" PRId16 " of %03" PRId16 "    ", gen_logGetEventCount(theLog), gen_logGetLogSize(theLog));
			if( ix < 0 || ix >= (*stopn)) goto commonExit;
			strptr += ix;
			*stopn -= ix;				// subtract off what we copied so far.
			//"Size: 00000 / 00000 "
			ix=snprintf(strptr, (*stopn), "Size: %05" PRId16 " / %05" PRId16 " ", gen_logGetEventCount(theLog)*sizeof(logEntry_t), gen_logGetLogSize(theLog)*sizeof(logEntry_t));
			if( ix < 0 || ix >= (*stopn)) goto commonExit;
			strptr += ix;
			*stopn -= ix;				// subtract off what we copied so far.

			begin++;					// next will output log entry [0]
		}
		else goto commonExit;// only a footer, begin was -2

	}
	item = theLog->logHead + begin;
									// now copy the log entries
	while((nmbr-begin)>0)
	{
		if(*stopn < (2*LOG_LINE_LENGTH)+1)
			break;					// stop here, not enough room for rest of log.
		if(item >= theLog->logSize)
		{
			item -= theLog->logSize;		// wrap around: item is our head pointer.
		}
		tm = theLog->logEntry[item].timeStamp;	// get timestamp (or data)
		if( theLog->logEntry[item].logEvent == L_DATABYTES )
		{
			if(begin == 0 )
				goto log_next;	// skip this if first in log, log wrapped around and the description was lost.
			// output: "  4294967295" // 12 characters
			ix=snprintf(strptr, (*stopn), " %11" PRId32 , tm);
			if( ix < 0 || ix >= (*stopn)){
				// problem encountered
				break;	//goto commonExit;
			}
			strptr += ix;
			*stopn -= ix;				// subtract off what we copied so far.
		}else if((theLog->logEntry[item].logEvent == L_DATABYTES_HEX || theLog->logEntry[item].logEvent == L_EVENTCODE_HEX ))
		{
			if(begin == 0 )
				goto log_next;	// skip this if first in log, log wrapped around and the description was lost.

			// output: " 0xXXXXXXXX" // 12 characters
			ix=snprintf(strptr, (*stopn), "  0x%08" PRIX32 , tm);
			if( ix < 0 || ix >= (*stopn)){
				// problem encountered
				break;	//goto commonExit;
			}
			strptr += ix;
			*stopn -= ix;				// subtract off what we copied so far.
			// output event codes if that is the data type.
			if(theLog->logEntry[item].logEvent == L_EVENTCODE_HEX )
			{
#if LOG_HAS_EVENT_FLAGS
				// output: " nnn"  repeated as needed.
				uint16_t codes = ec_updateNumericLog(tm);
				codes = ec_iterError();
				while(codes)
				{
					ix=snprintf(strptr, (*stopn), " %3" PRId16 , codes);
					if( ix < 0 || ix >= (*stopn)){
						// problem encountered
						goto commonExit;
					}
					strptr += ix;
					*stopn -= ix;				// subtract off what we copied so far.
					codes = ec_iterError();
#endif
			}
		}else if( theLog->logEntry[item].logEvent ==  L_DATABYTES_STR)
		{
			if(begin == 0 )
				goto log_next;	// skip this if first in log, log wrapped around and the description was lost.

			// output: "  |cccc|      "	// 14 characters
			char* str = (char*) (&theLog->logEntry[item].timeStamp);	// string in time stamp.
			ix=snprintf(strptr, (*stopn), s_EOL "  |%c%c%c%c|      "  , (char)(str[0]), (char)(str[1]), (char)(str[2]), (char)(str[3]));
			if( ix < 0 || ix >= (*stopn)){
				// problem encountered
				break;	//goto commonExit;
			}
			strptr += ix;
			*stopn -= ix;				// subtract off what we copied so far.
		}else if(theLog->logEntry[item].logEvent == L_DATABYTES_CSTR)
		{
			if(begin == 0 )
				goto log_next;	// skip this if first in log, log wrapped around and the description was lost.
			// output: " <the_string>"
			const char* file =  (const char*) (theLog->logEntry[item].timeStamp);
			if(file == NULL || *file < ' ' || *file > '~'){
				file = NULL;
			}
			ix=snprintf(strptr, (*stopn), " %s",(file != NULL)? file : "NULL");
			if( ix < 0 || ix >= (*stopn)){
				// problem encountered
				goto log_next;	// skip this entry.
			}
			strptr += ix;
			*stopn -= ix;				// subtract off what we copied so far.
		}else if((logEntry[item].logData24b & LOG_NO_TIMESTAMP) == 0
				&& logEntry[item].logEvent != L_VERSION && logEntry[item].logEvent != L_NO_OP)
		{	// standard log event
			// output "\nhhhh:mm:ss.ttt"	// 14 characters
			ttt = tm %1000;		// miliseconds part.
			tm = tm/1000;		// timestamp to seconds.
			hh = tm /(60*60);	// hours
			tm -= (hh*3600);	// remove the hours
			mm = tm/60;			// minutes
			ss = tm - (mm*60);	// remaining is seconds.
			ix=snprintf(strptr, (*stopn), s_EOL "%04" PRId16 ":%02" PRId16 ":%02" PRId16 ".%03" PRId16 , hh,mm,ss,ttt);
			if( ix < 0 || ix >= (*stopn))
			{
				// problem encountered
				break;	//goto commonExit;
			}
			strptr += ix;
			*stopn -= ix;				// subtract off what we copied so far.
		}
#if (LOG_ENABLE_TYPE_INFO == 1)
			// now " XX" hex event code:
		ix=snprintf(strptr, (*stopn), " 0x%02" PRIX16 , (uint16_t)(theLog->logEntry[item].logEvent));
		if( ix < 0 || ix >= (*stopn))
		{
			// problem encountered
			break;	//goto commonExit;
		}
		strptr += ix;
		*stopn -= ix;				// subtract off what we copied so far.
#endif
			// insert log description " ...................."
		ix=snprintf(strptr, (*stopn), " %s", logGetDescription(theLog->logEntry[item].logEvent));
		if( ix < 0 || ix >= (*stopn))
		{
			// problem encountered
			break;	//goto commonExit;
		}
		strptr += ix;
		*stopn -= ix;				// subtract off what we copied so far.
			// dump any 16 bit data
		if(theLog->logEntry[item].logData24b & LOG_D16B_USED)
		{
			if(theLog->logEntry[item].logData24b & LOG_D16B_HEX_OUT)
			{
				ix=snprintf(strptr, (*stopn), "  0x%04" PRIX16 , (uint16_t)(theLog->logEntry[item].logData24b & LOG_D16B_DATA_MSK));
				if( ix < 0 || ix >= (*stopn))
				{
					// problem encountered
					break;	//goto commonExit;
				}
			}else if(theLog->logEntry[item].logData24b & LOG_D16B_UNSIGNED)
			{
				ix=snprintf(strptr, (*stopn), "  %5" PRIu16 , (uint16_t)(theLog->logEntry[item].logData24b & LOG_D16B_DATA_MSK));
				if( ix < 0 || ix >= (*stopn))
				{
					// problem encountered
					break;	//goto commonExit;
				}
			}else if(theLog->logEntry[item].logData24b & LOG_D16B_CHAR)
			{
				char c1,c2;
				c1 = (char)((theLog->logEntry[item].logData24b & LOG_D16B_DATA_MSK) &0x0ff);
				c2 = (char)((theLog->logEntry[item].logData24b & LOG_D16B_DATA_MSK)>>8) &0x0ff;
				c1 = c1 == 0 ? ' ' : c1;	// filter NULLs
				c2 = c2 == 0 ? ' ' : c2;
				ix=snprintf(strptr, (*stopn), "  %c%c" , c1,c2);
				if( ix < 0 || ix >= (*stopn))
				{
					// problem encountered
					break;	//goto commonExit;
				}
			}else
			{
				// print value as signed 16 bit value.
				ix=snprintf(strptr, (*stopn), "  %5" PRId16 , (int16_t)(theLog->logEntry[item].logData24b & LOG_D16B_DATA_MSK));
				if( ix < 0 || ix >= (*stopn))
				{
					// problem encountered
					break;	//goto commonExit;
				}
			}
			strptr += ix;
			*stopn -= ix;			// subtract off what we copied so far.
		}else if(theLog->logEntry[item].logEvent == L_VERSION)
		{
			// timestamp has version info

			// output: "  X.XX.XXXX " // 12 characters
			ix=snprintf(strptr, (*stopn), " %2" PRIX32 ".%02" PRIX32 ".%04" PRIX32 " ", tm>>24, (tm&0x00ff0000)>>16, (tm&0x0ffff));
			if( ix < 0 || ix >= (*stopn)){
				// problem encountered
				break;	//goto commonExit;
			}
			strptr += ix;
			*stopn -= ix;				// subtract off what we copied so far.
		}
log_next:
			// Done with this log entry
		item++;
		begin++;					// keep track
	}/* end of while loop */

commonExit:
	*strptr=0;			// terminate string
	return begin;		// return the log entry we stopped at.
}

/* ************************************************************************* */
#endif	/*	#ifndef LOG_COMPACT	*/
/* ************************************************************************* */


#ifdef LOG_COMPACT
#define LOG_LINE_LENGTH 34
/*!
 * logGetStringLength
 *
 * returns how long the log string will be for the requested number of log entries + uptime header line.
 * 	If nmbr == 0 then just returns the length of one log line
 * Used for web page builder to set Content-Length: parameter.
 * Can send ~ 36 log entries per packet.
 * HTTP text response header GetLogResponse[] is 78 bytes long (max).
 *
 * 14+20 is the length of each log line. 34
 * '14+20' needs to be adjusted if log string is changed.
 *
 * 34 is the length of the first header line (Uptime).
 * Includes end-of-line termination, Does Not include NULL termination.
 */
int16_t logGetStringLength(int16_t nmbr)
{
#ifdef LOG_DESCRIPTIONS
	if(nmbr==0)
		return LOG_LINE_LENGTH;		// don't include header length.

	return (nmbr*LOG_LINE_LENGTH)+LOG_LINE_LENGTH;	// includes "uptime" header.
#else
	return 0;
#endif
}

/*!
 * logGetString
 *
 * Builds an ASCII string from log structure data, placing the string in str[].
 * Starts at log entry 'begin' for nmbr entries or until stopn chars copied.
 *    stopn has the size of the passed in string buffer (not including room for NULL)
 * Each log entry is 34 bytes long: "hh:mm:ss XX ......................\r\n"
 * Does NOT NULL terminate string.
 *
 * Returns log entry we stopped at (if == nmbr then done.)
 *   subtracts from *stopn the number of bytes copied
 *
 */
int16_t logGetString(int16_t begin, int16_t nmbr, char* str, int16_t* stopn )
{
#ifdef LOG_DESCRIPTIONS
	int16_t item = logHead + begin;
	uint16_t tm;			// tm - timestamp
	uint16_t hr,mn,sc;		// hr - hours, mm - minutes, sc - seconds
	uint16_t len =  0;		// keeps track of total bytes copied to output str.
	char * strptr;
	strptr=str;

#if( LOG_ENABLE_UPTIME == 1)
	if( begin == 0)
	{
		// put in uptime
		// "\r\nUptime: Days:0000 Hours:00    \r\n" len=34
		strptr = strcpystr((unsigned char*)strptr,(unsigned char*)"\r\nUptime: Days:",&len);// 15 bytes (ST=15)
		strptr = IntToAsciiDec(strptr, 4, (uint32_t)upTimeDays);		// 4 bytes (ST=19)
		*strptr++=' ';														// 1 bytes (ST=20
		len+=5;
		hr = LogFile[logTail].timeStamp/(3600);		// L_END will have additonal hours since last 12 hour check point.
		strptr = strcpystr((unsigned char*)strptr,(unsigned char*)"Hours:",&len);			// 6 bytes (ST=26)
		strptr = IntToAsciiDec(strptr, 2, (uint32_t)(upTimeHours+hr)); // 2 bytes (ST=28)
		len+=2;
		*strptr++=' ';
		*strptr++=' ';
		*strptr++=' ';
		*strptr++=' ';														// 4 bytes (ST=32)
		len+=4;
		*strptr++='\r';														// 1
		*strptr++='\n';														// 1 bytes (Total=34 for header)
		len+=2;
		*stopn -= len;				// subtract off what we copied so far.
	}
#endif

									// now copy the log entries
	while((nmbr-begin)>0)
	{
		if(*stopn < LOG_LINE_LENGTH)
			break;					// stop here, not enough room for rest of log.
		if(item >= LOG_FILE_SIZE)
		{
			item -= LOG_FILE_SIZE;		// wrap around: item is our head pointer.
		}
		tm = LogFile[item].timeStamp;
		if(LogFile[item].logEvent == L_DATABYTES )
		{	// output: "   XXXXX"
			*strptr++=' ';
			*strptr++=' ';
			*strptr++=' ';											//  3 bytes long
			len+=3;
			strptr = IntToAsciiDec(strptr, 5, (uint32_t)tm); 		// line 8 bytes long
			len+=5;
		}else if(LogFile[item].logEvent == L_DATABYTES_HEX )
		{	// output: "  0xXXXX"
			*strptr++=' ';
			*strptr++=' ';
			*strptr++='0';
			*strptr++='x';											//  4 bytes long
			len+=4;
			strptr = IntToAsciiHex(strptr, 4, (uint32_t)tm); 	// line 8 bytes long
			len+=4;
		}else if( LogFile[item].logEvent ==  L_DATABYTES_STR)
		{
			// output: "  |cc|  "
			*strptr++=' ';
			*strptr++=' ';
			*strptr++='|';
			*strptr++=tm&0x0ff;
			*strptr++=(tm>>8);
			*strptr++='|';
			*strptr++=' ';
			*strptr++=' ';											// line 8 bytes long

			len+=8;

		}else
		{	// standard log event
			// output "hh:mm:ss XX"
			hr = tm /(60*60);
			tm -= (hr*3600);				// remove the hours
			mn = tm/60;						// minutes
			sc = tm - (mn*60);				// remaining is seconds.
			strptr = IntToAsciiDec(strptr, 2, (uint32_t)hr);
			*strptr++=':';											// line 3 bytes
			len+=3;
			strptr = IntToAsciiDec(strptr, 2, (uint32_t)mn);
			*strptr++=':';
			len+=3;
			strptr = IntToAsciiDec(strptr, 2, (uint32_t)sc);	// line 8 bytes
			len+=2;
		}
					// now " XX" hex event code:
		*strptr++=' ';
		len+=1;
		strptr = IntToAsciiHex(strptr, 2, (uint32_t)LogFile[item].logEvent);
		*strptr++=' ';												// line 12 bytes long
		len+=3;
					// insert log description
		strptr = strcpystr(strptr, (char*)logGetDescription(LogFile[item].logEvent), &len); // +20 = 32 bytes
					// terminate this line.
		*strptr++='\r';
		*strptr++='\n';												// line totally 34 bytes long each log event.
		len+=2;
					// done with this one....
		*stopn -= LOG_LINE_LENGTH;	//*** Note: adjust macro if what we copy above is changed ********
		item++;
		begin++;													// keep track
	}
//	*strptr=0;			// terminate string (might be dangerous)
	return begin;		// return the log entry we stopped at.
#else
	return nmbr;		// description option not built.
#endif	/*	#ifdef LOG_DESCRIPTIONS	*/
}

#endif	/*	#ifdef LOG_COMPACT  */

/* ------------------------------------------------------------------------- */
/* -------------------------- Log File --END-------------------------------- */
/* ------------------------------------------------------------------------- */



/* -------- Helper functions ----------------------------------------------- */

#ifdef LOG_DESCRIPTIONS
// *******************************************************************************
// strcpystr
// copy input string to destination, does not copy end of string (null).
// returns the next position after last char copied in dest.
// Adds to *len with how many copied.
char * strcpystr(char* dest, char* input, uint16_t* len)
{
	/*  */
	while(*input)
	{
		*dest++ = *input++;
		*len = *len +1;
	}
	return dest;
}


/* Converts a 0x00-0x0F number to ascii '0'-'F' */
#define HexToAscii(hex) (uint8_t)( ((hex) & 0x0F) + ((((hex) & 0x0F) <= 9) ? '0' : ('A'-10)) )

/*****************************************************************************
Name:	IntToAsciiHex
Parameters:
		dest_string
			Pointer to a buffer will the string will reside
		min_digits
			Specifies the minimum number of characters the output string will
			have. Leading zeros will be written as '0' characters.
			Maximum of 8 digits (long)
Returns:
		A pointer to the string's NULL character in the string that was just
		created.
Description:
		This function is used to convert a passed in unsigned long into an ASCII
		string represented in Hexadecimal format.
*****************************************************************************/
char * IntToAsciiHex(char * dest_string, int min_digits, uint32_t value)
{
	uint8_t i;
	uint8_t total_digits = 0;
	char buff[10];
	if(min_digits > 8)
		min_digits = 8;	// prevent failure below.

	for(i=0;i<8;i++)
	{
		buff[i] = HexToAscii(value);
		value = value >> 4;

		if(buff[i] != '0')
			total_digits = i+1;
	}

	if( total_digits < 	min_digits)
		total_digits = min_digits;

	// copy hex digits to destination.
	i = total_digits;
	while(i)
	{
		*dest_string++ = buff[i-1];
		i--;
	}

	*dest_string = 0;

	return dest_string;
}
/*****************************************************************************
Name:	IntToAsciiDec
Parameters:
		dest_string
			Pointer to a buffer will the string will reside
		min_digits
			Specifies the minimum number of characters the output string will
			have. Leading zeros will be written as '0' characters.  Max = 7.
Returns:
		A pointer to the string's NULL character in the string that was just
		created.
Description:
		This function is used to convert a passed in unsigned long into an ASCII
		string represented in base 10 decimal format.
*****************************************************************************/
char * IntToAsciiDec(char * dest_string, int min_digits, uint32_t value)
{
	const unsigned long base10[] = {1,10,100,1000,10000,100000,1000000,10000000,100000000};

	uint8_t i;
	uint32_t tmp;
	uint8_t total_digits = 0;
	char buff[10];

	if(min_digits > 7)
		min_digits = 7;	// prevent failure below.
	for(i=0;i<7;i++)
	{
		tmp = value % base10[i+1];
		value -= tmp;

		buff[i] = tmp / base10[i];
		buff[i] += '0';		// make it ASCII

		if(buff[i] != '0')
			total_digits = i+1;
	}

	if( total_digits < 	min_digits)
		total_digits = min_digits;

	i = total_digits;
	while(i)
	{
		*dest_string++ = buff[i-1];
		i--;
	}

	*dest_string = 0;

	return dest_string;
}
#endif	// #ifdef LOG_DESCRIPTIONS


/* ------------------------------------------------------------------------- */
/* -------------------------- Log File --END-------------------------------- */
/* ------------------------------------------------------------------------- */

