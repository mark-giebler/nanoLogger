/**
 ******************************************************************************
 * @file           log.h
 * @brief          NanoLogger - a compact embedded logging system.
 ******************************************************************************
 *
 *  Created on: Mar-02-2013
 *      @author Mark Giebler
 *
 * NanoLogger a compact and memory frugal embedded logging system for embedded systems.
 * This logger was created for a home project:  Mark's 20,000KM Garage Door Controller project.
 *
 * By Mark Giebler mark.giebler-VIA-gmail---com
 *
 * 2013-03-02
 *
 * This log file code is open source, Apache 2.0 license.
 */
#ifndef NANO_LOG_H
#define NANO_LOG_H
#include <stdint.h>
#include <stddef.h>

/* ------------------------------------------------------------------------- */
/* -------------------------- Log File --BEGIN------------------------------ */
/* ------------------------------------------------------------------------- */

#define LOG_COMPACT					//!< define if using compact model: 24 bit data, one second time stamp resolution
#define LOG_DESCRIPTIONS			//!< define if log event to string conversion is enabled. Used for human readable output.
#define LOG_FIXED_STRING_LENGTH		//!< define if log description strings are fixed length.
#define LOG_RESET_PERSISTENT		//!< define if RAM log file is persistent across MCU resets. Only available in non-compact model.
#define LOG_FILE_SIG	0x47656562	//!< Signature to indicate structure was initialized. I.e. A POR has already occurred. Used with @ref LOG_RESET_PERSISTENT. */

#define LOG_HEARTBEAT				//!< define if periodic heart-beat event should be logged. Default period is 12 hours, see LOG_HEARTBEAT_TIME in log.c

#define LOG_END_EVENT			(0)	//!< set to 1 to enable logging an L_LOG_END event at the tail of the log. Adds CPU cycles
#define LOG_ENABLE_TYPE_INFO	(0)	//!< set to 1 to enable log type strings (HEX, DEC, etc) in log output
#define LOG_ENABLE_UPTIME		(0)	//!< set to 1 to enable log file's internal up time timer

#define LOG_ENABLE_CALLBACKS	(1) //!< set to 1 to enable callbacks for periodic log maintenance


#define LOG_HAS_EVENT_FLAGS		(0) //!< Set to 1 to enable support for logging 32 bit event flags. This is an extension to nanoLog. Not available in @ref LOG_COMPACT model.

#define LOG_HAS_TEST_METHODS	(1) //!< Set to 1 to enable support for log file test functions.


// non-compact model:  macros for handling logData24b field. Lower 16 bits allocated for data, next 8 bits (16:23) for flags.
#define LOG_D16B_USED		(0x00800000ul)	//!< logData24b field has 16bit data (default is signed data)
#define LOG_D16B_HEX_OUT	(0x00400000ul)	//!< output 24b data as unsigned 16 bit HEX value
#define LOG_D16B_UNSIGNED	(0x00200000ul)	//!< output 24b data as unsigned 16 bit DEC value.
#define LOG_D16B_CHAR		(0x00100000ul)	//!< output 24b data as 2 ASCII characters.
#define LOG_NO_TIMESTAMP	(0x00010000ul)	//!< Log entry does not have a timestamp value (for compressed log events)
#define LOG_D16B_DATA_MSK	(0x0000FFFFul)	//!< mask to get 16 bit data field

/*! \brief  Define the log message types enum.
 *
 * - The elements of this enum are application specific.
 * - The elements of this enum define the events that can be logged.
 * - The elements of this enum must be manually coordinated with a matching string in logDescriptionList[]
 * in the same order.
 */
typedef enum {
	L_NO_OP			=0x00,		//!< Place holder.
	L_RESET_POR		=0x01,		//!< =0x1 Power On Reset.
	L_RESET_PIN,				//!< =0x2 Reset pin activated.
	L_RESET_SW,					//!< =0x3 Software initiated reset.
	L_IWDG_RESET			,	//!< IWDG initiated reset.
	L_SHELL_RESET,				//!< Reset initiated via Shell UI
	L_VERSION,					//!< Log firmware version. Example how to log: `logDataRaw(L_VERSION, get_sw_version_int());`
	L_VERSION_HW,				//!< Log hardware revision.
	L_FLASH_OK				,
	L_FLASH_INIT			,
	L_MAIN_LP_START			,	//!< Main loop starting.
	L_PPP_UP				,
	L_PPP_DOWN				,

	L_ASSERT_FAIL			,	//!< Assertion failure.

	L_ERROR_HANDLER			,	//!< Error_Handler was called.

	L_LOG_INIT_OK		=0xF0,	//!< =0xF0 Logging system init OK.
	L_LOG_CLEARED		=0xF1,	//!< LOG file was cleared.
	L_LOG_END			=0xF2,	//!< marks the end of log.
	L_TEST_LOG			=0xF3,	//!< for testing logging.
	L_LOG_PAUSED		=0xF4,
	L_LOG_RESUMED		=0xF5,
	L_LOG_REPEATED		=0xF6,
	L_LOG_RESERVED		=0xF7,
	L_EVENT_BITS		=0xF8,	//!< Log 32 bit event flags.
	L_UNHANDLED			=0xF9,	//!< Function handler not fully implemented on LOG_COMPACT model.
	L_DATABYTES_CSTR	=0xFA,	//!< =0xFA data in timestamp word, const char const pointer to null terminated string.
	L_EVENTCODE_HEX		=0xFB,	//!< 32 bit eventflag bits stored in timeStamp word, output as HEX.
	L_DATABYTES_STR		=0xFC,	//!< =0xFC data string, next 10 entries...
	L_DATABYTES_HEX		=0xFD,	//!< data in timeStamp bytes, output as HEX.
	L_DATABYTES			=0xFE,	//!< indicates the timeStamp bytes are data for the previous log event. Output as Decimal.
	L_HEARTBEAT			=0xFF	//!< Periodic heart beat.
} logType;
typedef uint8_t logTypeEvent;

#ifdef LOG_COMPACT
/*!
 * Define the structure for what makes a log entry.
 * Option for timestamp resolution and log data option.
 *
 * One second time stamp resolution. 8 bit aligned, each log entry 24 bits.
 */
typedef struct {
	uint8_t logEvent;
	uint16_t timeStamp;				//!< unit seconds. Start at zero on reset, rolls over at 12 hours (43200)
									//!< also can be used for pertinent data related to a previous log entry.
} logEntry_t;

extern logEntry_t LogFile[];		//!< logging data array.
extern int16_t logHead;				//!< index to head.
extern int16_t logTail;				//!< index to tail.
extern uint16_t logTime;			//!< one second tick counter. Rolls over at 12 hours.
extern uint16_t  upTimeHours;		//!< total up time in hours.
extern uint16_t  upTimeDays;		//!< total up time days.
void LogTimeDo( void);				//!< update log's one second timer if needed.
void logDataRaw(logType Levent, uint16_t Rdata);
void logData(uint16_t Ldata);
void logEvent(logType Levent);
void logDataHex(uint16_t Ldata);
int16_t logGetEventCount(void);
int16_t logGetString(int16_t begin, int16_t nmbr, char* str, int16_t* stopn );

void logEventDataHex(logType Levent, uint16_t Ldata);

__attribute__((always_inline)) inline void logResetFile(void){ return; }

__attribute__((always_inline)) inline void log_Init(void){ return; }


#define logEventData16bHex_noTimestamp(e,d) logDataRaw(e,d)
#define logEventData16bDec_noTimestamp(e,d) logDataRaw(e,d)
#define logEventData16buDec_noTimestamp(e,d) logDataRaw(e,d)
#define logDataDec(d) logDataRaw(L_DATABYTES,d)

// Define dummy functions that are only fully available in the non-compact model.
#define logEventData16buDecConstString(E, l, f) {logEvent(L_UNHANDLED);}
#define logEventData16bChar(E,c)	 {logEvent(L_UNHANDLED);}
#define logEventData16bDec(E,d)	 {logEvent(L_UNHANDLED);}
#define logPause(a)	;

#endif	/*	#ifdef LOG_COMPACT	*/

#ifndef LOG_COMPACT
/*!
 * Define the structure for what makes a log entry.
 * Option for timestamp resolution and log data option.
 *
 * Millisecond time stamp resolution, 32 bit aligned, each log entry 64 bits.
 */
typedef struct {
	uint32_t logEvent : 8;		//!< logType enum, constrained to 8 bits.
	uint32_t logData24b : 24;	//!< 16 bit log data. Bit 23: data present. Bit 22: Hex output, Bit 21: Unsigned dec output, Bit 20:16 unused. Bit 0:15 data.
	uint32_t timeStamp;			//!< unit 1's of milliseconds. Start at zero on reset, rolls over at 4294967.295 seconds => 1193.046389 hours => 49 days, 17:02:47.295.
} logEntry_t;

/**
 * @brief Define the log  file header typedef.
 *
 */
typedef struct {
	volatile uint32_t logSignature;		//!< used to determine if log structure needs to be initialized.
	volatile uint16_t logSize;	//!< size of logEntry array.
	volatile int16_t logHead;	//!< index to head.
	volatile int16_t logTail;	//!< index to tail.
	int16_t logTrimBack;		//!< amount to trim the log by when it becomes full.
#if (LOG_ENABLE_CHECKCODE)
	volatile uint16_t checkCode;//!< check code to validate tail/head pointers.
#endif
	logEntry_t *logEntry;		//!< logging data array.
} logFile_t;
extern logFile_t logFile;		//  __attribute__ ((section (".noinit")));
extern uint32_t HAL_GetTick(void);
#define logTime HAL_GetTick()	//!< get current one second or XX milliSecond clock tick.
void log_Init(void);			//!< Must be call first.
void logTimeDo( uint32_t timeNow );
void logResetFile(void);

int16_t logGetStringLength(int16_t nmbr);

#if LOG_HAS_EVENT_FLAGS
	void logSetEventFilter(uint32_t mask);	//Todo: _mg_ add 32bit event flag support if needed.
#endif

#endif	/*	#ifndef LOG_COMPACT	*/


#if( LOG_ENABLE_UPTIME == 1)
void logTimeSync( void);			//!< set new 1 second threshold tick.
#endif

#if (LOG_ENABLE_CALLBACKS == 1)
void logRegisterCallback(void (*callback)(uint32_t));
#endif


#ifndef LOG_COMPACT
// non-compact log

void gen_logDataRaw(logType Levent, uint32_t Rdata, uint32_t data24b,  logFile_t *  __restrict__ theLog);
/*!
 * Log a log event (logType Levent) to a specific log file (theLog)
 */
__attribute__((always_inline)) inline void gen_logEvent(logType Levent,  logFile_t *  __restrict__ theLog)
{
	gen_logDataRaw( Levent, logTime, 0, theLog );	// thread safe
}
/*!
 * Log a logType event with data (raw format) to the main log file.
 * @param Levent the event enum @ref logType to log.
 * @param Rdata is logged in the timestamp field.
 */
__attribute__((always_inline)) inline void logDataRaw(logType Levent, uint32_t Rdata)
{
	gen_logDataRaw(Levent, Rdata, 0, &logFile);
}
/*!
 * Log a logType event with data (raw format) to the main log file
 * including 24 bit data.
 * @param Levent the event enum @ref logType to log.
 * @param Rdata is logged in the timestamp field.
 */
__attribute__((always_inline)) inline void logData24bRaw(logType Levent, uint32_t Rdata, uint32_t data24b)
{
	gen_logDataRaw(Levent, Rdata, data24b, &logFile);
}

__attribute__((always_inline)) inline void logDataDec(uint32_t Ldata)
{
	logDataRaw( L_DATABYTES, Ldata );
}
__attribute__((always_inline)) inline void logDataHex(uint32_t Ldata)
{
	logDataRaw( L_DATABYTES_HEX, Ldata );
}

/*!
 * Log a logType event to the main log file.
 * @param Levent the event enum @ref logType to log.
 */
__attribute__((always_inline)) inline void logEvent(logType Levent)
{
	logDataRaw( Levent, logTime );
}

/*!
 * Log an event with pertinent 32 bit data to the main log file.
 * Data logged such that it will be displayed as a decimal number
 * on the same line as the Levent description.
 *
 * Use to log one value. If you want to log multiple values in one line,
 * then you need to do:
 *
 * ~~~
 * logEvent( Levent );	// The event description text.
 * logDataDec( Ldata );	// first value
 * logDataDec( Ldata );	// second value
 * logDataDec( Ldata ); // third value
 * . . . etc.
 * ~~~
 *
 */
__attribute__((always_inline)) inline  void logEventDataDec(logType Levent, uint32_t Ldata)
{
	logEvent( Levent );
	logDataDec( Ldata );
}
/*!
 * log an event with pertinent 32 bit data to the main log file.
 * Data logged such that it will be displayed as a hexadecimal number
 * on the same line as the Levent description.
 *
 * Use to log one value. If you want to log multiple values in one line,
 * then you need to do:
 *
 * ~~~
 * logEvent( Levent );	// The event description text.
 * logDataHex( Ldata );	// first value
 * logDataHex( Ldata );	// second value
 * logDataHex( Ldata ); // third value
 * . . . etc.
 * ~~~
 *
 */
__attribute__((always_inline)) inline void logEventDataHex(logType Levent, uint32_t Ldata)
{
	logEvent( Levent );
	logDataHex( Ldata );
}

#ifndef LOG_FIXED_STRING_LENGTH
void logEventData16buDecConstString(logType Levent,uint16_t numb, const char * Ldata);
void logEventDataConstStr(logType Levent, const char * Ldata);
/*!
 * Log a pointer to a const char array that contains a null terminated string.
 * This uses only one log file entry.
 * @attention passed in string must be const! and must not change!
 */
__attribute__((always_inline)) inline void logDataConstString(const char * Ldata)
{
	logDataRaw( L_DATABYTES_CSTR, (uint32_t)Ldata );
}
#else
// Define stubbed-out functions if features not enabled.
#define logEventData16buDecConstString(...)	 {logEvent(L_UNHANDLED);}
#define logEventDataConstStr(...)	 {logEvent(L_UNHANDLED);}
#define logDataConstString(...)	 {logEvent(L_UNHANDLED);}
#endif	/*	#ifndef LOG_FIXED_STRING_LENGTH	*/


#if LOG_HAS_TEST_METHODS
void gen_logTestFillEmpty(logFile_t * __restrict__ theLog);

/*!
 * gen_logTestFillEmpty()
 * Fill empty space in the log with test entry with incrementing test number
 *
 */
__attribute__((always_inline)) inline static void logTestFillEmpty(void)
{
	gen_logTestFillEmpty(&logFile);
}

#endif

/*!
* log event with 16 bit data.
* This uses only one log file entry.
* Data will be dumped in hexadecimal.
* thread safe.
*/
__attribute__((always_inline)) inline void logEventData16bHex(logType Levent, uint16_t Ldata)
{
	logData24bRaw(Levent,logTime, LOG_D16B_USED | LOG_D16B_HEX_OUT | (((uint32_t) Ldata)&LOG_D16B_DATA_MSK));
}

/*!
* log event with 16 bit data without timestamp.
* This uses only one log file entry.
* Data will be dumped in hexadecimal.
* thread safe.
*/
__attribute__((always_inline)) inline void logEventData16bHex_noTimestamp(logType Levent, uint16_t Ldata)
{
	logData24bRaw(Levent,logTime, LOG_NO_TIMESTAMP | LOG_D16B_USED | LOG_D16B_HEX_OUT | (((uint32_t) Ldata)&LOG_D16B_DATA_MSK));
}

/*!
 * log event with 16 bit data.
 * This uses only one log file entry.
 * Data will be dumped in decimal (signed).
 * thread safe.
 */
__attribute__((always_inline)) inline void logEventData16bDec(logType Levent, int16_t Ldata)
{
	logData24bRaw(Levent,logTime, LOG_D16B_USED | (((uint32_t) Ldata)&LOG_D16B_DATA_MSK));
}

/*!
 * log event with 16 bit data without timestamp.
 * This uses only one log file entry.
 * Data will be dumped in decimal (signed).
 * thread safe.
 */
__attribute__((always_inline)) inline void logEventData16bDec_noTimestamp(logType Levent, int16_t Ldata)
{
	logData24bRaw(Levent,logTime, LOG_NO_TIMESTAMP | LOG_D16B_USED | (((uint32_t) Ldata)&LOG_D16B_DATA_MSK));
}
/*!
 * log event with 16 bit data.
 * This uses only one log file entry.
 * Data will be dumped in decimal (unsigned).
 * thread safe.
 */
__attribute__((always_inline)) inline void logEventData16buDec(logType Levent, uint16_t Ldata)
{
	logData24bRaw(Levent,logTime, LOG_D16B_USED | LOG_D16B_UNSIGNED | (((uint32_t) Ldata)&LOG_D16B_DATA_MSK));
}

/*!
 * log event with 16 bit data without timestamp.
 * This uses only one log file entry.
 * Data will be dumped in decimal (unsigned).
 * thread safe.
 */
__attribute__((always_inline)) inline void logEventData16buDec_noTimestamp(logType Levent, uint16_t Ldata)
{
	logData24bRaw(Levent,logTime, LOG_NO_TIMESTAMP | LOG_D16B_USED | LOG_D16B_UNSIGNED | (((uint32_t) Ldata)&LOG_D16B_DATA_MSK));
}

/*!
 * log event with 2 ASCII characters.
 * This uses only one log file entry.
 * Additional Data will be dumped as a two ASCII characters Ldata[0] Ldata[1].
 * thread safe.
 */
__attribute__((always_inline)) inline void logEventData16bChar(logType Levent, const char* Ldata)
{
	logData24bRaw(Levent,logTime, LOG_D16B_USED | LOG_D16B_CHAR | (((uint32_t) Ldata[0] | ((uint32_t)Ldata[1])<<8)&LOG_D16B_DATA_MSK));
}

/*!
 * Pause or resume a log file.
 * Pause = 1  pause logging, Pause != 1 resume log.
 */
__attribute__((always_inline)) inline void gen_logPause(uint8_t pause,  logFile_t *  __restrict__ theLog )
{
	if( pause == 1 )
	{
//		gen_logEvent(L_PAUSED, theLog );
		theLog->logSignature = ~LOG_FILE_SIG;
	}else{
		theLog->logSignature = LOG_FILE_SIG;
//		gen_logEvent(L_RESUMED, theLog );
	}
}


void logEventDataStr(logType Levent, char* str);

int16_t gen_logGetLogSize(logFile_t * __restrict__ theLog);
int16_t gen_logGetEventCount(logFile_t * __restrict__ theLog);

/*!
 * get the log file maximum capacity size.
 */
__attribute__((always_inline)) inline int16_t logGetLogSize(void)
{
	return gen_logGetLogSize(&logFile);
}

/*!
 * LogEventCount
 *
 * Returns the number of log events currently in the log logFile.
 */
__attribute__((always_inline)) inline int16_t logGetEventCount(void)
{
	return gen_logGetEventCount(&logFile);
}

const char * logGetDescription(logTypeEvent levent);

int16_t gen_logGetString(int16_t begin, int16_t nmbr, char* str, int16_t * stopn, logFile_t * __restrict__ theLog );

/*!
 * logGetString
 *
 * Builds ASCII string from log logFile data placing string in str[].
 * Starting at log entry 'begin' (if -1, start with current timestamp  header, if -2 output a timestamp footer and exit.)
 * for nmbr entries (or until L_END) or until stopn chars copied.
 * str - buffer to copy log info to.
 *    stopn has the size of the passed in string buffer str[].
 * Each log entry is minimum of 40 bytes long: "hhhh:mm:ss.ttt XX .........20.........\n"
 * 18 for timestamp prefix + 20 for message +1 to EOL char
 * Returns log entry we stopped at (== nmbr then done.)
 *   subtracts number of bytes copied from *stopn
 *
 * Not thread safe! Should be called within OS critical region protection.
 * Do not call from ISR.
 */
__attribute__((always_inline)) inline int16_t logGetString(int16_t begin, int16_t nmbr, char* str, int16_t* stopn )
{
	return gen_logGetString(begin, nmbr, str, stopn, &logFile);
}
int16_t logGetStringLength(int16_t nmbr);

/*!
 * Pause or resume the system log file.
 * @param pause = 1  pause logging, pause != 1 resume log
 */
__attribute__((always_inline)) inline void logPause(uint8_t pause)
{
	gen_logPause( pause, &logFile);
}
#endif	/*	#ifndef LOG_COMPACT	*/




/* ------------------------------------------------------------------------- */
/* -------------------------- Log File --END-------------------------------- */
/* ------------------------------------------------------------------------- */


#endif	/*	NANO_LOG_H */
